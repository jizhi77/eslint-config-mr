# eslint-config-mr

> An custom ESLint shareable config for MR FE. 

## Feature

- 包装ESLint依赖，新项目只需安装单个配置相关的包
- 统一ESLint配置，提供围绕[standard](https://standardjs.com/)的基础配置
- 省却ESLint工具学习及配置使用时间

## Install

```shell
npm i -D eslint @mr/eslint-config-mr husky lint-staged
```

> 对于使用 `babel` 编译的项目，安装并指定解析器 `babel-eslint`。
```shell
npm i -D babel-eslint
```
```js
parserOptions: { parser: 'babel-eslint' }
```

## 使用

- [JavaScript项目](./docs/JavaScript.md)
- [Vue项目](./docs/Vue.md)
- [React项目](./docs/React.md)

## TODO

- [ ] Prettier
