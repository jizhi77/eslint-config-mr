
## JavaScript 配置文档

#### .eslintrc.js

```js
module.exports = {
  extends:'@mr/eslint-config-mr'
}
```

#### package.json

```json
{
  "scripts":{
    "lint": "eslint --ext .js ./ --fix"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.{js}": [
      "npm run lint",
      "git add"
    ]
  }
}
```

