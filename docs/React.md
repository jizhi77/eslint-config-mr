
## React

#### .eslintrc.js

```js
module.exports = {
  extends:'@mr/eslint-config-mr/react'
}
```

#### package.json

```json
{
  "scripts":{
    "lint": "eslint --ext .js,.jsx ./ --fix"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.{js,jsx}": [
      "npm run lint",
      "git add"
    ]
  }
}
```