
## Vue

#### .eslintrc.js

```js
module.exports = {
  extends:'@mr/eslint-config-mr/vue'
}
```

#### package.json

```json
{
  "scripts":{
    "lint": "eslint --ext .js,.vue ./ --fix"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.{js,vue}": [
      "npm run lint",
      "git add"
    ]
  }
}
```
