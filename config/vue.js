const baseConfig = require('./base')

module.exports = {
    extends: ['standard', 'plugin:vue/essential'],
    plugins: ['vue'],
    rules: Object.assign(baseConfig.rules, {

    }),
}
