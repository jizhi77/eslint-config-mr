const baseConfig = require('./base')

module.exports = {
    extends: ['standard'],
    rules: Object.assign(baseConfig.rules, {

    }),
}
