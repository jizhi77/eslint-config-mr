const baseConfig = require('./base')

module.exports = {
    extends: ['standard', 'standard-react'],
    plugins: ['react-hooks'],
    rules: Object.assign(baseConfig.rules, {

    }),
}
